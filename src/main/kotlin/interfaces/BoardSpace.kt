package interfaces

import domain.boardGame.Player

interface BoardSpace {
    val boardSpaceValue: Int
    fun getRule() : String
    fun applyRule(player: Player, boardSpaces: List<BoardSpace>)
}
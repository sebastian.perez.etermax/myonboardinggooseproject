package domain.boardGame

import domain.boardSpaces.BridgeBoardSpace
import domain.boardSpaces.CommonBoardSpace
import domain.boardSpaces.DeathBoardSpace
import domain.boardSpaces.FinishBoardSpace
import domain.boardSpaces.ForwardBoardSpace
import domain.boardSpaces.HotelBoardSpace
import domain.boardSpaces.MazeBoardSpace
import domain.boardSpaces.PrisonBoardSpace
import domain.boardSpaces.WellBoardSpace
import interfaces.BoardSpace

class GooseGameBuilder {
    private var boardSpaces = mutableListOf<BoardSpace>()
    private var maxBoardSpaces: Int = 0

    fun createBoardSpaces(maxValueBoardSpace: Int): List<BoardSpace>{
        this.maxBoardSpaces = maxValueBoardSpace
        createSpecialRulesForOnlyOneBoardSpace()
        createCommonBoardSpaces()
        return boardSpaces
    }

    private fun createSpecialRulesForOnlyOneBoardSpace() {
        createTheBridgeBoardSpace(6)
        createTheHotelBoardSpace(19)
        createTheWellBoardSpace(31)
        createTheMazeBoardSpace(42)
        createDeathBoardSpace(58)
        createFinishBoardSpace(63)
        createThePrisonBoardSpaces(listOf(50, 55))
        createForwardBoardSpacesRule()
    }

    private fun createFinishBoardSpace(boardSpace: Int) = boardSpaces.add(FinishBoardSpace(boardSpace))

    private fun createDeathBoardSpace(boardSpace: Int) = boardSpaces.add(DeathBoardSpace(boardSpace))

    private fun createTheMazeBoardSpace(boardSpace: Int) = boardSpaces.add(MazeBoardSpace(boardSpace))

    private fun createTheWellBoardSpace(boardSpace: Int) = boardSpaces.add(WellBoardSpace(boardSpace))

    private fun createTheHotelBoardSpace(boardSpace: Int) = boardSpaces.add(HotelBoardSpace(boardSpace))

    private fun createTheBridgeBoardSpace(boardSpace: Int) = boardSpaces.add(BridgeBoardSpace(boardSpace))

    private fun createThePrisonBoardSpaces(newBoardSpaces: List<Int>) {
        for(bs in newBoardSpaces)
            boardSpaces.add(PrisonBoardSpace(bs))
    }

    private fun createForwardBoardSpacesRule() {
        for(i in 1..maxBoardSpaces){
            if(!isInBoardSpaces(i) && i%6==0)
                boardSpaces.add(ForwardBoardSpace(i))
        }
    }

    private fun createCommonBoardSpaces() {
        for(i in 0..maxBoardSpaces){
            if(!isInBoardSpaces(i))
                boardSpaces.add(CommonBoardSpace(i))
        }
    }

    private fun isInBoardSpaces(boardSpace: Int) =
        (boardSpaces.filter { bs ->  bs.boardSpaceValue == boardSpace}).any()
}
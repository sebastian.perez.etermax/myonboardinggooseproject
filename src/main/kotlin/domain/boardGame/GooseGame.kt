package domain.boardGame

import interfaces.BoardSpace

class GooseGame (builder: GooseGameBuilder, players: List<Player>, maxBoardSpaces: Int){
    private var maxBoardSpaces: Int
    private var boardSpaces = listOf<BoardSpace>()
    private var players = listOf<Player>()
    private var currentPlayer = Player()

    init {
        boardSpaces = builder.createBoardSpaces(maxBoardSpaces)
        this.players = players
        this.maxBoardSpaces = maxBoardSpaces
    }

    fun playGame() {
        while (getPlayersInRun().isNotEmpty()){
            nextTurn()
            move(currentPlayer)
            applyRuleForPlayer(currentPlayer)
        }
    }

    fun getPlayersInRun(): List<Player> = players.filter { p -> !p.isWinner() && !p.isInPrison() && !p.isInWell()}

    fun nextTurn() {
        if(players.none { p -> p.isMyTurn() })
            (players.minByOrNull { p -> p.getPlayerId() })?.setTurn(true)
        else
            isTurnOf(getNextPlayerToPlay(currentPlayer.getPlayerId()))
    }

    fun getNextPlayerToPlay(playerWhoIsPlayingId: Int): Player? {
        return if(isTheLastPlayerInRun(playerWhoIsPlayingId)){
            getPlayerWhoCanPlay(getFirstPlayerInRun())
        }else{
            getPlayerWhoCanPlay(getNextPlayerInPlayWithIdBigger(playerWhoIsPlayingId))
        }
    }

    fun isTheLastPlayerInRun(playerWhoIsPlayingId: Int): Boolean = !getPlayersInRun().any { p -> p.getPlayerId() > playerWhoIsPlayingId }

    fun getFirstPlayerInRun(): Player? = getPlayersInRun().minByOrNull { p -> p.getPlayerId() }

    private fun getNextPlayerInPlayWithIdBigger(playerId: Int): Player? = getPlayersInRun().filter{ p -> p.getPlayerId() > playerId}.minByOrNull { p -> p.getPlayerId() }

    private fun getPlayerWhoCanPlay(player: Player?): Player? {
        if (player != null && !player.canPlay())
            return getNextPlayerToPlay(player.getPlayerId())
        return player
    }

    fun isTurnOf(player: Player?) {
        for (p in players){
            if(p.getPlayerId() == player?.getPlayerId())
                setTurnForPlayer(p)
            else
                p.setTurn(false)
        }
    }

    private fun setTurnForPlayer(player: Player) {
        currentPlayer = player
        player.setTurn(true)
    }

    fun move(player: Player){
        val nextBoardSpace = getBoardSpace(player.getBoardSpace().boardSpaceValue + player.throwDice())
        player.setBoardSpace(nextBoardSpace)
    }

    private fun applyRuleForPlayer(player: Player) {
        printAppliedRuleForPlayer(player)
        player.getBoardSpace().applyRule(player, boardSpaces)
    }

    private fun printAppliedRuleForPlayer(player: Player) = println("Player_" + player.getPlayerId() + ": " + player.getBoardSpace().getRule())

    fun getRuleFromBoardSpace(boardSpace: Int): String {
        if(boardSpace > maxBoardSpaces)
            return "Move to space 53 and stay in prison for two turns"
        return boardSpaces.first { bs -> bs.boardSpaceValue == boardSpace }.getRule()
    }
    fun getBoardSpaces(): List<BoardSpace> = boardSpaces

    fun getBoardSpace(boardSpaceVal: Int): BoardSpace {
        if(boardSpaceVal > maxBoardSpaces) {
            return boardSpaces.first { bs -> bs.boardSpaceValue == 53 }
        }
        return boardSpaces.first { bs -> bs.boardSpaceValue == boardSpaceVal }
    }
}
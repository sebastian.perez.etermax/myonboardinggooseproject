package domain.boardGame

import domain.boardSpaces.CommonBoardSpace
import domain.boardSpaces.PrisonBoardSpace
import domain.boardSpaces.WellBoardSpace
import interfaces.BoardSpace

class Player(playerId: Int = 0) {

    private var missTurn: Boolean = false
    private var win: Boolean = false
    private var id: Int
    private var boardSpace: BoardSpace
    private var myTurn: Boolean = false

    init {
        id = playerId
        this.boardSpace = CommonBoardSpace(0)
    }

    fun isMyTurn(): Boolean = myTurn

    fun setTurn(isMyTurn: Boolean) {
        myTurn = isMyTurn
    }

    fun getPlayerId(): Int = id

    fun isWinner(): Boolean = win

    fun setBoardSpace(newBoardSpace: BoardSpace) {
        boardSpace = newBoardSpace
    }

    fun throwDice(): Int = (2..12).random()

    fun getBoardSpace(): BoardSpace = this.boardSpace

    fun setLikWinner() {
        win = true
    }

    fun canPlay(): Boolean {
        if(missTurn){
            setMissTurn(false)
            return false
        }
        if(isInPrison())
            return false
        if(isInWell())
            return false
        return true
    }

    fun setMissTurn(missTurn: Boolean) {
        this.missTurn = missTurn
    }

    fun isInPrison(): Boolean = boardSpace is PrisonBoardSpace && (boardSpace as PrisonBoardSpace).getPlayerInPrison() == id

    fun isInWell(): Boolean = boardSpace is WellBoardSpace && (boardSpace as WellBoardSpace).getPlayerInWell() == id
}

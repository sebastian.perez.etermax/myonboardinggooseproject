package domain.boardSpaces

import domain.boardGame.Player
import interfaces.BoardSpace

class CommonBoardSpace(override val boardSpaceValue: Int) : BoardSpace {
    override fun getRule(): String = String.format("Stay in space %s", boardSpaceValue.toString())
    override fun applyRule(player: Player, boardSpaces: List<BoardSpace>) {

    }
}
package domain.boardSpaces

import domain.boardGame.Player
import interfaces.BoardSpace

class BridgeBoardSpace(override val boardSpaceValue: Int) : BoardSpace {
    override fun getRule(): String = "The Bridge: Go to space 12"
    override fun applyRule(player: Player, boardSpaces: List<BoardSpace>) {
        val nextBoardSpace = boardSpaces.first { bs -> bs.boardSpaceValue == 12 }
        player.setBoardSpace(nextBoardSpace)
    }
}
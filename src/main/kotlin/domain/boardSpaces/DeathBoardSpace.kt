package domain.boardSpaces

import domain.boardGame.Player
import interfaces.BoardSpace

class DeathBoardSpace(override val boardSpaceValue: Int) : BoardSpace {
    override fun getRule(): String = "Death: Return your piece to the beginning - start the game again"
    override fun applyRule(player: Player, boardSpaces: List<BoardSpace>) {
        val nextBoardSpace = boardSpaces.first() { bs -> bs.boardSpaceValue == 0}
        player.setBoardSpace(nextBoardSpace)
    }
}
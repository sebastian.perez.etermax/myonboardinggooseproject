package domain.boardSpaces

import domain.boardGame.Player
import interfaces.BoardSpace

class PrisonBoardSpace(override val boardSpaceValue: Int) : BoardSpace {
    private var playerInPrison: Player = Player()

    override fun getRule(): String = "The Prison: Wait until someone comes to release you - they then take your place"
    override fun applyRule(player: Player, boardSpaces: List<BoardSpace>) {
        playerInPrison = player
    }

    fun getPlayerInPrison(): Int {
        return playerInPrison.getPlayerId()
    }
}
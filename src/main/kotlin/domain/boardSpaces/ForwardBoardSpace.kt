package domain.boardSpaces

import domain.boardGame.Player
import interfaces.BoardSpace

class ForwardBoardSpace(override val boardSpaceValue: Int) : BoardSpace {
    override fun getRule(): String = "Move two spaces forward."
    override fun applyRule(player: Player, boardSpaces: List<BoardSpace>) {
        val nextBoardSpace = boardSpaces.first { bs -> bs.boardSpaceValue == player.getBoardSpace().boardSpaceValue + 2 }
        player.setBoardSpace(nextBoardSpace)
    }
}
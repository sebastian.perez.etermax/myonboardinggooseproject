package domain.boardSpaces

import domain.boardGame.Player
import interfaces.BoardSpace

class MazeBoardSpace(override val boardSpaceValue: Int) : BoardSpace {
    override fun getRule(): String = "The Maze: Go back to space 39"
    override fun applyRule(player: Player, boardSpaces: List<BoardSpace>) {
        val nextBoardSpace = boardSpaces.first { bs -> bs.boardSpaceValue == 39 }
        player.setBoardSpace(nextBoardSpace)
    }
}
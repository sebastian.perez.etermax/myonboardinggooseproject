package domain.boardSpaces

import domain.boardGame.Player
import interfaces.BoardSpace

class FinishBoardSpace(override val boardSpaceValue: Int) : BoardSpace {
    override fun getRule(): String = "Finish: you ended the game"
    override fun applyRule(player: Player, boardSpaces: List<BoardSpace>) {
        player.setLikWinner()
    }
}
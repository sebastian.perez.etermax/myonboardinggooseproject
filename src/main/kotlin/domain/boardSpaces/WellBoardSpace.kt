package domain.boardSpaces

import domain.boardGame.Player
import interfaces.BoardSpace

class WellBoardSpace(override val boardSpaceValue: Int) : BoardSpace {
    private var playerInWell = Player()

    override fun getRule(): String = "The Well: Wait until someone comes to pull you out - they then take your place"
    override fun applyRule(player: Player, boardSpaces: List<BoardSpace>) {
        playerInWell = player
    }

    fun getPlayerInWell(): Int {
        return playerInWell.getPlayerId()
    }
}
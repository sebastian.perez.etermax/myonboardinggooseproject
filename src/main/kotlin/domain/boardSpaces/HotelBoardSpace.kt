package domain.boardSpaces

import domain.boardGame.Player
import interfaces.BoardSpace

class HotelBoardSpace(override val boardSpaceValue: Int) : BoardSpace {
    override fun getRule(): String = "The Hotel: Stay for (miss) one turn"
    override fun applyRule(player: Player, boardSpaces: List<BoardSpace>) {
        player.setMissTurn(true)
    }
}
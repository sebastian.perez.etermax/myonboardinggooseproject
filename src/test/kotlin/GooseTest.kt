import domain.boardGame.GooseGame
import domain.boardGame.GooseGameBuilder
import domain.boardGame.Player
import org.junit.Before
import org.junit.Test
import kotlin.test.assertContains
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertNotEquals

class GooseTest {
    private lateinit var gameBuilder: GooseGameBuilder
    private lateinit var game: GooseGame
    private lateinit var players: MutableList<Player>
    private var maxBoardSpaces: Int = 0


    @Before
    fun setup(){
        maxBoardSpaces = 63
        players = mutableListOf()
        createPlayers(4)
        gameBuilder = GooseGameBuilder()
        game = GooseGame(gameBuilder, players, maxBoardSpaces)
    }

    @Test
    fun showBoardSpacesRules(){
        assertRules(2, "Stay in space 2")
        assertRules(6, "The Bridge: Go to space 12")
        assertRules(12, "Move two spaces forward.")
        assertRules(13, "Stay in space 13")
        assertRules(18, "Move two spaces forward.")
        assertRules(19, "The Hotel: Stay for (miss) one turn")
        assertRules(31, "The Well: Wait until someone comes to pull you out - they then take your place")
        assertRules(42, "The Maze: Go back to space 39")
        assertRules(50, "The Prison: Wait until someone comes to release you - they then take your place")
        assertRules(55, "The Prison: Wait until someone comes to release you - they then take your place")
        assertRules(58, "Death: Return your piece to the beginning - start the game again")
        assertRules(63, "Finish: you ended the game")
        assertRules(64, "Move to space 53 and stay in prison for two turns")
        assertRules(74, "Move to space 53 and stay in prison for two turns")
    }

    @Test
    fun getPlayersInRun_allPlayersAreInRun() = assertEquals(players.count(), game.getPlayersInRun().count())

    @Test
    fun getPlayersInRun_playerOneAndFourAreInRun(){
        getPlayer(2).setLikWinner()
        putPlayerInPrison(3)
        assertEquals(2, game.getPlayersInRun().count())
        assertEquals(1, game.getPlayersInRun().minOf { p -> p.getPlayerId() })
        assertEquals(4, game.getPlayersInRun().maxOf { p -> p.getPlayerId() })
    }

    @Test
    fun getPlayersInRun_noPlayersInRun(){
        getPlayer(1).setLikWinner()
        getPlayer(2).setLikWinner()
        putPlayerInPrison(3)
        putPlayerInWell(4)
        assertEquals(0, game.getPlayersInRun().count())
    }

    @Test
    fun isTurnOfPlayerTwo_nextTurnIsTurnOfPlayerThree(){
        setTurnOfPlayer(2)
        runTurns(1)

        assertFalse(getPlayer(1).isMyTurn())
        assertFalse(getPlayer(2).isMyTurn())
        assert(getPlayer(3).isMyTurn())
        assertFalse(getPlayer(4).isMyTurn())
    }

    @Test
    fun isTurnOfPlayerTwo_runThreeTurn_nowIsTurnOfPlayerOne(){
        setTurnOfPlayer(2)
        runTurns(3)

        assert(getPlayer(1).isMyTurn())
        assertFalse(getPlayer(2).isMyTurn())
        assertFalse(getPlayer(3).isMyTurn())
        assertFalse(getPlayer(4).isMyTurn())
    }

    @Test
    fun getLastPlayerInRun_allPlayersAreInRun_lastPlayerIsPlayerFour(){
        assertFalse(game.isTheLastPlayerInRun(getPlayer(1).getPlayerId()))
        assertFalse(game.isTheLastPlayerInRun(getPlayer(2).getPlayerId()))
        assertFalse(game.isTheLastPlayerInRun(getPlayer(3).getPlayerId()))
        assert(game.isTheLastPlayerInRun(getPlayer(4).getPlayerId()))
    }

    @Test
    fun getFirstPlayerInRun_allPlayersAreInRun_firstPlayerIsPlayerOne() = assertEquals(1, game.getFirstPlayerInRun()?.getPlayerId())

    @Test
    fun getFirstPlayerInRun_threePlayersAreInRun_playerOneWin_firstPlayerIsPlayerTwo(){
        getPlayer(1).setLikWinner()
        assertEquals(2, game.getFirstPlayerInRun()?.getPlayerId())
    }

    @Test
    fun getNextPlayerInPlay_allPlayersAreInRun(){
        assertEquals(2, game.getNextPlayerToPlay(1)?.getPlayerId())
        assertEquals(3, game.getNextPlayerToPlay(2)?.getPlayerId())
        assertEquals(4, game.getNextPlayerToPlay(3)?.getPlayerId())
        assertEquals(1, game.getNextPlayerToPlay(4)?.getPlayerId())
    }

    @Test
    fun moveFirstTime_boardSpaceBetweenTwoAndTwelve(){
        setTurnOfPlayer(1)
        val player = getPlayer(1)
        game.move(player)
        assertContains((2..12), player.getBoardSpace().boardSpaceValue)
    }

    @Test
    fun playARealGame(){
        game.playGame()
    }

    @Test
    fun playARealGame_onePlayer(){
        createPlayers(1)
        val myGame = GooseGame(gameBuilder, players, maxBoardSpaces)
        myGame.playGame()
    }

    @Test
    fun playARealGame_twoPlayer(){
        createPlayers(2)
        val myGame = GooseGame(gameBuilder, players, maxBoardSpaces)
        myGame.playGame()
    }

    @Test
    fun playARealGame_tenPlayer(){
        createPlayers(10)
        val myGame = GooseGame(gameBuilder, players, maxBoardSpaces)
        myGame.playGame()
    }

    private fun putPlayerInPrison(playerId: Int) {
        val player = getPlayer(playerId)
        player.setBoardSpace(game.getBoardSpace(50))
        player.getBoardSpace().applyRule(player, game.getBoardSpaces())
    }

    private fun putPlayerInWell(playerId: Int) {
        val player = getPlayer(playerId)
        player.setBoardSpace(game.getBoardSpace(31))
        player.getBoardSpace().applyRule(player, game.getBoardSpaces())
    }

    private fun setTurnOfPlayer(id: Int) = game.isTurnOf(players.first { p -> p.getPlayerId() == id})

    private fun runTurns(times: Int) {
        for (i in 1..times)
            game.nextTurn()
    }

    private fun getPlayer(id: Int) = players.first { p -> p.getPlayerId() == id }

    private fun createPlayers(numberOfPlayers: Int) {
        players.clear()
        for (i in 1..numberOfPlayers){
            players.add(Player(i))
        }
    }

    private fun assertRules(boardSpace: Int, expectedRule: String){
        assertEquals(expectedRule, game.getRuleFromBoardSpace(boardSpace))
    }
}


import domain.boardGame.GooseGame
import domain.boardGame.GooseGameBuilder
import domain.boardGame.Player
import org.junit.Before
import org.junit.Test
import kotlin.test.assertEquals
import kotlin.test.assertFalse

class BoardSpaceTest {
    private lateinit var game: GooseGame
    private lateinit var gameBuilder: GooseGameBuilder
    private lateinit var players: MutableList<Player>
    private var maxBoardSpaces: Int = 0

    @Before
    fun setup(){
        maxBoardSpaces = 63
        players = mutableListOf()
        createPlayers(4)
        gameBuilder = GooseGameBuilder()
        game = GooseGame(gameBuilder, players, maxBoardSpaces)
    }

    @Test
    fun applyBridgeRule(){
        applyBoardSpaceRuleForPlayer(1, 6)
        assertEquals(12, getBoardSpace(1))
    }

    @Test
    fun applyMazeRule(){
        applyBoardSpaceRuleForPlayer(1, 42)
        assertEquals(39, getBoardSpace(1))
    }

    @Test
    fun applyDeathRule(){
        applyBoardSpaceRuleForPlayer(1, 58)
        assertEquals(0, getBoardSpace(1))
    }

    @Test
    fun applyFinishRule(){
        applyBoardSpaceRuleForPlayer(1, maxBoardSpaces)
        assert(getPlayer(1).isWinner())
    }

    @Test
    fun applyForwardRule(){
        applyBoardSpaceRuleForPlayer(1, 12)
        applyBoardSpaceRuleForPlayer(2, 18)
        assertEquals(14, getBoardSpace(1))
        assertEquals(20, getBoardSpace(2))
    }

    @Test
    fun applyHotelRule_turnOfPlayerOne_finishOneRound_turnOfPlayerTwo(){
        setTurnOfPlayer(1)
        applyBoardSpaceRuleForPlayer(1, 19)
        runTurns(players.count())
        assertFalse(getPlayer(1).isMyTurn())
        assert(getPlayer(2).isMyTurn())
    }

    @Test
    fun applyPrisonRule_turnOfPlayerOne_finishOneRound_turnOfPlayerTwo(){
        setTurnOfPlayer(1)
        applyBoardSpaceRuleForPlayer(1, 50)
        runTurns(players.count())
        assertFalse(getPlayer(1).isMyTurn())
        assert(getPlayer(2).isMyTurn())
    }

    @Test
    fun applyPrisonRule_turnOfPlayerOneApplyPrison_turnOfPlayerTwoApplySamePrison_turnOfPlayerOneAfterThreeTurns(){
        setTurnOfPlayer(1)
        applyBoardSpaceRuleForPlayer(1, 50)
        runTurns(1)
        setTurnOfPlayer(2)
        applyBoardSpaceRuleForPlayer(2, 50)
        runTurns(3)
        assert(getPlayer(1).isMyTurn())
    }

    @Test
    fun applyPrisonRule_turnOfPlayerOneApplyPrison_turnOfPlayerTwoApplySamePrison_turnOfPlayerThreeAfterOneRound(){
        setTurnOfPlayer(1)
        applyBoardSpaceRuleForPlayer(1, 50)
        runTurns(1)
        setTurnOfPlayer(2)
        applyBoardSpaceRuleForPlayer(2, 50)
        runTurns(players.count())
        assert(getPlayer(3).isMyTurn())
    }

    @Test
    fun applyPrisonRule_turnOfPlayerOneApplyPrison_turnOfPlayerTwoApplyDifferentPrison_turnOfPlayerThreeAfterThreeTurns(){
        setTurnOfPlayer(1)
        applyBoardSpaceRuleForPlayer(1, 50)
        runTurns(1)
        setTurnOfPlayer(2)
        applyBoardSpaceRuleForPlayer(2, 55)
        runTurns(3)
        assert(getPlayer(3).isMyTurn())
    }

    @Test
    fun applyWellRule_turnOfPlayerOne_turnOfPlayerTwoAfterOneRound(){
        setTurnOfPlayer(1)
        applyBoardSpaceRuleForPlayer(1, 31)
        runTurns(players.count())
        assertFalse(getPlayer(1).isMyTurn())
        assert(getPlayer(2).isMyTurn())
    }

    @Test
    fun applyRule_boardSpaceBiggerThanMaxValue(){
        setTurnOfPlayer(1)
        applyBoardSpaceRuleForPlayer(1, maxBoardSpaces + 1)

        assertEquals(53, getPlayer(1).getBoardSpace().boardSpaceValue)
    }

    private fun applyBoardSpaceRuleForPlayer(playerId: Int, boardSpaceVal: Int) {
        setBoardSpace(playerId, boardSpaceVal)
        applyRule(playerId)
    }

    private fun getBoardSpace(playerId: Int) = getPlayer(playerId).getBoardSpace().boardSpaceValue

    private fun applyRule(playerId: Int) = getPlayer(playerId).getBoardSpace().applyRule(getPlayer(playerId), game.getBoardSpaces())

    private fun setBoardSpace(playerId: Int, boardSpaceVal: Int) = getPlayer(playerId).setBoardSpace(game.getBoardSpace(boardSpaceVal))

    private fun runTurns(times: Int) {
        for (i in 1..times)
            game.nextTurn()
    }

    private fun getPlayer(id: Int) = players.first { p -> p.getPlayerId() == id }

    private fun setTurnOfPlayer(id: Int) = game.isTurnOf(players.first { p -> p.getPlayerId() == id})

    private fun createPlayers(numberOfPlayers: Int) {
        players.clear()
        for (i in 1..numberOfPlayers){
            players.add(Player(i))
        }
    }
}